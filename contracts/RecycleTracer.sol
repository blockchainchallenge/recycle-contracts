pragma solidity < 0.6.0;

contract RecycleTracer {
    address public contractOwner;

    uint constant CLIENT_TYPE_RECYCLER = 1;
    uint constant CLIENT_TYPE_RECOLECTOR = 2;
    uint constant CLIENT_TYPE_EMPLOYER = 3;
    uint constant CLIENT_TYPE_RECYCLE_FACTORY = 4;

    struct RecycledPacket {
        uint _id;
        address _generator;
        address _owner;
        uint _value;
        uint _reward;
        address[] _clientTrack;
    }

    struct Client {
        uint _id;
        uint _type;
        uint[] _generatedRecycledPackagesId;
        uint[] _carryRecycledPackagesId;
    }

    mapping(address => Client) clients;
    address[] clientsAddress;

    RecycledPacket[] recycledPackets;

    constructor() public {
        contractOwner = msg.sender;
    }

    function createClient(
        address userAddress,
        uint clientType
    ) public {
        Client memory client = Client(
            clientsAddress.length,
            clientType,
            new uint[](0),
            new uint[](0)
        );

        clientsAddress.push(userAddress);
        clients[userAddress] = client;
    }

    // Retrive all the client data
    function getClient(
        address clientAddress
    )
        public
        view
        returns (
            uint id,
            uint clientType,
            uint generatedRecycledPackages,
            uint carryRecycledPackages
        )
    {
        id = 0;
        clientType = 0;
        generatedRecycledPackages = 0;
        carryRecycledPackages = 0;

        if(clients[clientAddress]._type > 0) {
            id = clients[clientAddress]._id;
            clientType = clients[clientAddress]._type;
            generatedRecycledPackages = clients[clientAddress]._generatedRecycledPackagesId.length;
            carryRecycledPackages = clients[clientAddress]._carryRecycledPackagesId.length;
        }
    }

    // Retrieve the carryed packages by the users
    function getUserCarryPackets(
        address userAddress
    )
        public
        view
        returns (
            uint carryPackets
        )
    {
        if (clients[userAddress]._type > 0) {
            return clients[userAddress]._carryRecycledPackagesId.length;
        }

        return 0;
    }

    // This function is called by the machine when the user put all the recycled items
    function createPacket(
        address recolectorAddress,
        uint packetValue,
        uint reward
    ) public {
        address generatorAddress = msg.sender;

        RecycledPacket memory recycledPacket = RecycledPacket(
            recycledPackets.length,
            generatorAddress,
            recolectorAddress,
            packetValue,
            reward,
            new address[](0)
        );

        if (clients[generatorAddress]._type > 0) {
            clients[generatorAddress]._generatedRecycledPackagesId.push(recycledPacket._id);
        }

        if (clients[recolectorAddress]._type > 0) {
            clients[recolectorAddress]._carryRecycledPackagesId.push(recycledPacket._id);
        }
        recycledPackets.push(recycledPacket);

        recycledPackets[recycledPackets.length - 1]._clientTrack.push(recolectorAddress);
    }

    // the employer recolect the packages from the recolector
    function recolectRecyclePackets(
        address recolectorAddress
    ) public {
        address employerAddress = msg.sender;
        
        require(
            clients[employerAddress]._type == CLIENT_TYPE_EMPLOYER,
            "Recolect is only allowe by employees"
        );

        require(
            clients[recolectorAddress]._type == CLIENT_TYPE_RECOLECTOR,
            "Only can recolect on recolectors machines"
        );

        for (
            uint i = 0;
            i < clients[recolectorAddress]._carryRecycledPackagesId.length;
            ++i
        ) {
            uint packageId = clients[recolectorAddress]._carryRecycledPackagesId[i];

            recycledPackets[packageId]._clientTrack.push(employerAddress);
            clients[employerAddress]._carryRecycledPackagesId.push(packageId);
        }

        clients[recolectorAddress]._carryRecycledPackagesId.length = 0;
    }

    // the employer drop the package to the factory
    // function dropPackages() {}
}