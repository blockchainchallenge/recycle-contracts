const RecyCoin = artifacts.require("RecyCoin.sol");
const RecycleTracer = artifacts.require("RecycleTracer.sol");

module.exports = async function (deployer) {
    let tx;
    await deployer.deploy(RecyCoin);

    const recycleTransaction = await deployer.deploy(RecycleTracer);
    tx = await web3.eth.getTransactionReceipt(recycleTransaction.transactionHash);
    console.log("GAS USED FOR RECYCLE TRACER: " + tx.cumulativeGasUsed);
}