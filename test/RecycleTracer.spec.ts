contract("RecycleTracer", accounts => {
    const deployerAccount = accounts[0];
    const clientAddress = accounts[1];
    const recolectorAddress = accounts[2];
    const employerAddress = accounts[3];

    const userType = 1;
    const recolectorType = 2;
    const employerType = 3;
    const factoryType = 4;

    const RecycleTracerArtifact = artifacts.require("RecycleTracer");

    let recycleTracer;

    let createClientAndRecolector = async (clientAddress, recolectorAddress) => {
        await recycleTracer.createClient(clientAddress, userType);
        await recycleTracer.createClient(recolectorAddress, recolectorType);
    };

    let createClientAndRecolectorAndEmployer = async (clientAddress, recolectorAddress, employerAddress) => {
        await recycleTracer.createClient(clientAddress, userType);
        await recycleTracer.createClient(recolectorAddress, recolectorType);
        await recycleTracer.createClient(employerAddress, employerType);
    };

    beforeEach(async () => {
        recycleTracer = await RecycleTracerArtifact.new({
            from: deployerAccount,
        });
    });

    it("The contract is deployed successfully", () => {
        assert.ok(recycleTracer.address);
    });

    it("Create a new recycler client and retrieve the data", async () => {
        await recycleTracer.createClient(clientAddress, userType);

        const client = await recycleTracer.getClient(clientAddress);

        assert.equal(client.id, 0);
        assert.equal(client.clientType, userType);
        assert.equal(client.generatedRecycledPackages, 0);
        assert.equal(client.carryRecycledPackages, 0);
    });

    it("Create client and recolector", async () => {
        await createClientAndRecolector(clientAddress, recolectorAddress);

        const client = await recycleTracer.getClient(clientAddress);
        const recolector = await recycleTracer.getClient(recolectorAddress);

        assert.equal(client.id, 0);
        assert.equal(client.clientType, userType);
        assert.equal(client.generatedRecycledPackages, 0);
        assert.equal(client.carryRecycledPackages, 0);

        assert.equal(recolector.id, 1);
        assert.equal(recolector.clientType, recolectorType);
        assert.equal(recolector.generatedRecycledPackages, 0);
        assert.equal(recolector.carryRecycledPackages, 0);
    });

    it("Create client, recolector, employer, create package and recolect", async () => {
        await createClientAndRecolectorAndEmployer(clientAddress, recolectorAddress, employerAddress);
        await recycleTracer.createPacket(recolectorAddress, 3, 4, {
            from: clientAddress,
        });
        await recycleTracer.recolectRecyclePackets(recolectorAddress, {
            from: employerAddress,
        });

        const client = await recycleTracer.getClient(clientAddress);
        const recolector = await recycleTracer.getClient(recolectorAddress);
        const employer = await recycleTracer.getClient(employerAddress);

        assert.equal(client.generatedRecycledPackages, 1);
        assert.equal(client.carryRecycledPackages, 0);
        assert.equal(recolector.carryRecycledPackages, 0);
        assert.equal(recolector.generatedRecycledPackages, 0);
        assert.equal(employer.carryRecycledPackages, 1);
        assert.equal(employer.generatedRecycledPackages, 0);
    });
});
